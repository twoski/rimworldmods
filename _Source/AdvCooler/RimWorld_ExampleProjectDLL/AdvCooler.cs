﻿using System;
using UnityEngine;
using Verse;

namespace RimWorld
{
    public class Building_AdvCooler : Building_TempControl
    {
        private const float EfficiencyFalloffSpan = 100f;

        public Building_AdvCooler()
        {
        }

        public override void TickRare()
        {
            float single;
            if (this.compPowerTrader.PowerOn)
            {
                float ambientTemperature = base.AmbientTemperature;
                if (ambientTemperature <= -20f)
                {
                    single = (ambientTemperature >= -120f ? Mathf.InverseLerp(-120f, -20f, ambientTemperature) : 0f);
                }
                else
                {
                    single = 1f;
                }
                float props = this.compTempControl.Props.energyPerSecond * single * 4.16666651f;
                float single1 = GenTemperature.ControlTemperatureTempChange(base.Position, base.Map, props, this.compTempControl.targetTemperature);
                bool flag = !Mathf.Approximately(single1, 0f);
                CompProperties_Power compPropertiesPower = this.compPowerTrader.Props;
                if (!flag)
                {
                    this.compPowerTrader.PowerOutput = -compPropertiesPower.basePowerConsumption * this.compTempControl.Props.lowPowerConsumptionFactor;
                }
                else
                {
                    RoomGroup roomGroup = this.GetRoomGroup();
                    roomGroup.Temperature = roomGroup.Temperature + single1;
                    this.compPowerTrader.PowerOutput = -compPropertiesPower.basePowerConsumption;
                }
                this.compTempControl.operatingAtHighPower = flag;
            }
        }
    }
}