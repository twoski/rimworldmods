﻿using System;
using Verse;

namespace RimWorld
{
    public class CompProperties_SpawnerBugs: CompProperties
    {
        public float HiveSpawnPreferredMinDist = 3.5f;

        public float HiveSpawnRadius = 10f;

        public FloatRange HiveSpawnIntervalDays = new FloatRange(1.0f, 2.0f);
        //public FloatRange HiveSpawnIntervalDays = new FloatRange(0.1f, 0.11f);

        public CompProperties_SpawnerBugs()
        {
            this.compClass = typeof(CompSpawnerBugs);
        }
    }
}