﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RimWorld
{
    public class CompSpawnerBugs: ThingComp
    {
        private int nextHiveSpawnTick = -1;
        private bool spawnerActive = true;

        private CompProperties_SpawnerBugs Props
        {
            get
            {
                return (CompProperties_SpawnerBugs)this.props;
            }
        }

        public CompSpawnerBugs()
        {
        }

        public void SetActive(bool a)
        {
            spawnerActive = a;
        }

        Pawn SpawnPawn(Map map)
        {
            Pawn pawn = PawnGenerator.GeneratePawn(PawnKindDefOf.Spelopede, parent.Faction);
            GenSpawn.Spawn(pawn, CellFinder.RandomClosewalkCellNear(parent.Position, map, 3), map);

            return pawn;
        }

        void SpawnAggroBug(Map map)
        {
            Pawn bug = SpawnPawn(map);

            if (bug == null)
            {
                return;
            }

            bug.mindState.mentalStateHandler.TryStartMentalState(DefDatabase<MentalStateDef>.GetNamed("InsectManhunterPermanent"));
            Messages.Message("A predator insect has left its hive to hunt.", MessageTypeDefOf.ThreatSmall);
        }

        public void CalculateNextHiveSpawnTick()
        {
            Room room = this.parent.GetRoom();
            int num = 0;
            int num1 = GenRadial.NumCellsInRadius(9f);
            for (int i = 0; i < num1; i++)
            {
                IntVec3 position = this.parent.Position + GenRadial.RadialPattern[i];
                if (position.InBounds(this.parent.Map))
                {
                    if (position.GetRoom(this.parent.Map) == room)
                    {
                        if (position.GetThingList(this.parent.Map).Any<Thing>((Thing t) => t is Hive))
                        {
                            num++;
                        }
                    }
                }
            }
            float single = GenMath.LerpDouble(0f, 7f, 1f, 0.35f, (float)Mathf.Clamp(num, 0, 7));
            this.nextHiveSpawnTick = Find.TickManager.TicksGame + (int)(this.Props.HiveSpawnIntervalDays.RandomInRange * 60000f / (single * Find.Storyteller.difficulty.enemyReproductionRateFactor));
        }

        private bool CanSpawnHiveAt(IntVec3 c, float minDist, bool ignoreRoofedRequirement)
        {
            if (!ignoreRoofedRequirement && !c.Roofed(this.parent.Map) || !c.Standable(this.parent.Map) || minDist != 0f && c.DistanceToSquared(this.parent.Position) < minDist * minDist)
            {
                return false;
            }
            for (int i = 0; i < 8; i++)
            {
                IntVec3 intVec3 = c + GenAdj.AdjacentCells[i];
                if (intVec3.InBounds(this.parent.Map))
                {
                    List<Thing> thingList = intVec3.GetThingList(this.parent.Map);
                    for (int j = 0; j < thingList.Count; j++)
                    {
                        if (thingList[j] is Hive)
                        {
                            return false;
                        }
                    }
                }
            }
            if (c.GetThingList(this.parent.Map).Find((Thing x) => (x.def.category == ThingCategory.Building ? true : x.def.category == ThingCategory.Item)) != null)
            {
                return false;
            }
            return true;
        }

        public override string CompInspectStringExtra()
        {
            Hive hive = this.parent as Hive;

            if (hive != null && hive.active && spawnerActive)
            {
                return "Spawning predator insect in " + (this.nextHiveSpawnTick - Find.TickManager.TicksGame).ToStringTicksToPeriod();
            }
            else
            {
                return "Dormant";
            }
        }

        public override void CompTick()
        {
            Hive hive = this.parent as Hive;

            if (hive != null && hive.active && spawnerActive && Find.TickManager.TicksGame >= this.nextHiveSpawnTick)
            {
                SpawnAggroBug(this.parent.Map);
                this.CalculateNextHiveSpawnTick();
            }
        }

        public override void PostExposeData()
        {
            Scribe_Values.Look<int>(ref this.nextHiveSpawnTick, "nextHiveSpawnTick", 0, false);
        }

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            if (!respawningAfterLoad)
            {
                this.CalculateNextHiveSpawnTick();
            }
        }
    }
}