﻿
using System;
using System.Collections.Generic;
using System.Linq;

using RimWorld;

namespace Verse.AI
{
    public class MentalState_InsectManhunter: MentalState
    {
        public override void MentalStateTick()
        {
            base.MentalStateTick();
        }

        public override RandomSocialMode SocialModeMax()
        {
            return RandomSocialMode.Off;
        }
    }
}
