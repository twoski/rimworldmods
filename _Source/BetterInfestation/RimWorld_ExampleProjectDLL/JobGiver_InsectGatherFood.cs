﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
using Verse.AI;

namespace RimWorld
{
    class JobGiver_InsectGatherFood: ThinkNode_JobGiver
    {
        protected bool IgnoreForbid(Pawn pawn)
        {
            return true;
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            Pawn target = FindTarget(pawn);
            IntVec3 cell = FindCell(pawn);

            if (target == null || cell == IntVec3.Invalid)
            {
                return null;
            }

            if (!pawn.CanReserve(target))
            {
                return null;
            }

            return new Job(JobDefOf.HaulToCell, target as Thing, cell)
            {
                haulOpportunisticDuplicates = false,
                haulMode = HaulMode.ToCellNonStorage,
                count = 1
            };
        }

        IntVec3 FindCell(Pawn p)
        {
            List<Thing> hives = p.Map.listerThings.ThingsOfDef(ThingDefOf.Hive);

            if (!hives.Any())
            {
                return IntVec3.Invalid;
            }

            hives.Shuffle();

            foreach (Thing hive in hives)
            {
                for (int i = 0; i < 4; i++)
                {
                    IntVec3 pos = CellFinder.RandomClosewalkCellNear(hive.Position, p.Map, i + 1);

                    if (pos == IntVec3.Invalid || !p.CanReserve(pos))
                    {
                        continue;
                    }

                    return pos;
                }
            }

            return IntVec3.Invalid;
        }

        static bool AtHome(Pawn p)
        {
            List<Thing> hives = p.Map.listerThings.ThingsOfDef(ThingDefOf.Hive);

            if (!hives.Any())
            {
                return false;
            }

            foreach (Thing hive in hives)
            {
                int dist = IntVec3Utility.ManhattanDistanceFlat(hive.Position, p.Position);

                if (dist < 10)
                {
                    return true;
                }
            }

            return false;
        }

        public static Pawn FindTarget(Pawn pawn)
        {
            List<Pawn> allPawns = pawn.Map.mapPawns.AllPawnsSpawned;
            int count = allPawns.Count;

            if (count == 0)
            {
                return null;
            }

            List<Pawn> humans = new List<Pawn>();
            List<Pawn> animals = new List<Pawn>();

            foreach (Pawn p in allPawns)
            {
                if ((!p.Downed && !p.Dead) || AtHome(p))
                {
                    continue;
                }

                if (p.RaceProps.Humanlike)
                {
                    humans.Add(p);
                }
                else if (p.RaceProps.Animal)
                {
                    animals.Add(p);
                }
            }

            Pawn result = GetClosest(humans, pawn);

            if (result != null && Rand.Range(0f, 1f) > 0.4f)
            {
                return result;
            }

            result = GetClosest(animals, pawn);

            return result;
        }

        static Pawn GetClosest(List<Pawn> _pawns, Pawn _me)
        {
            int best = int.MaxValue;
            Pawn result = null;

            foreach (Pawn p in _pawns)
            {
                if (_me.CanReserve(p) && _me.CanReach(p, PathEndMode.Touch, Danger.Deadly, true, TraverseMode.PassAllDestroyableThings))
                {
                    int dist = IntVec3Utility.ManhattanDistanceFlat(_me.Position, p.Position);

                    if (dist < best)
                    {
                        best = dist;
                        result = p;
                    }
                }
            }

            return result;
        }
    }
}
