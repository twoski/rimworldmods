﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
using Verse.AI;

namespace RimWorld
{
    class JobGiver_InsectHunt: ThinkNode_JobGiver
    {
        protected bool IgnoreForbid(Pawn pawn)
        {
            return true;
        }

        private Job MeleeAttackJob(Pawn pawn, Thing target)
        {
            Job job = new Job(JobDefOf.AttackMelee, target)
            {
                maxNumMeleeAttacks = 8,
                expiryInterval = Rand.Range(1500, 2500),
                attackDoorIfTargetLost = true
            };

            return job;
        }

        private static Building FindTurretTarget(Pawn pawn)
        {
            Pawn pawn1 = pawn;
            Predicate<Thing> predicate = (Thing t) => t is Building;
            IntVec3 intVec3 = new IntVec3();
            return (Building)AttackTargetFinder.BestAttackTarget(pawn1, TargetScanFlags.NeedLOSToPawns | TargetScanFlags.NeedLOSToNonPawns | TargetScanFlags.NeedLOSToAll | TargetScanFlags.NeedReachable | TargetScanFlags.NeedThreat, predicate, 0f, 70f, intVec3, float.MaxValue, false);
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            Thing target = FindTarget(pawn);

            if (target != null && NearDownedEnemy(target, pawn))
            {
                return null;
            }

            if (target == null)
            {
                return null;
            }

            return MeleeAttackJob(pawn, target);
        }

        bool AtHome(Pawn me)
        {
            List<Thing> hives = me.Map.listerThings.ThingsOfDef(ThingDefOf.Hive);

            if (!hives.Any())
            {
                return false;
            }

            foreach (Thing hive in hives)
            {
                int dist = IntVec3Utility.ManhattanDistanceFlat(hive.Position, me.Position);

                if (dist < 10)
                {
                    return true;
                }
            }

            return false;
        }

        bool NearDownedEnemy(Thing target, Pawn me)
        {
            Pawn dead = JobGiver_InsectGatherFood.FindTarget(me);

            if (dead == null || target == null)
            {
                return false;
            }

            float dist = (float)IntVec3Utility.ManhattanDistanceFlat(target.Position, me.Position);
            float dist2 = (float)IntVec3Utility.ManhattanDistanceFlat(dead.Position, me.Position);

            if (dist2 * 1.6f < dist && dist > 20f)
            {
                return true;
            }

            return false;
        }

        public static Thing FindTarget(Pawn pawn)
        {
            List<Pawn> allPawns = pawn.Map.mapPawns.AllPawnsSpawned;
            int count = allPawns.Count;

            if (count == 0)
            {
                return null;
            }

            List<Pawn> humans = new List<Pawn>();
            List<Pawn> animals = new List<Pawn>();

            foreach (Pawn p in allPawns)
            {
                if (p.DestroyedOrNull() || p.Downed || p.Dead)
                {
                    continue;
                }

                if (p.Faction != null && p.Faction.def == FactionDefOf.Insect)
                {
                    continue;
                }
                
                if (p.RaceProps.Humanlike)
                {
                    humans.Add(p);
                }
                else if (p.RaceProps.Animal)
                {
                    animals.Add(p);
                }
            }

            Building turr = FindTurretTarget(pawn);
            Pawn result = GetClosest(humans, pawn);

            if (Rand.Range(0f, 1f) < 0.4f || result == null)
            {
                Pawn animal = GetClosest(animals, pawn);

                if (result == null)
                {
                    result = animal;
                }
                else if (animal != null && pawn.Position.DistanceToSquared(animal.Position) < pawn.Position.DistanceToSquared(result.Position))
                {
                    result = animal;
                }
            }

            if (result == null)
            {
                return turr;
            }

            if (turr == null)
            {
                return result;
            }

            if (pawn.Position.DistanceToSquared(result.Position) > pawn.Position.DistanceToSquared(turr.Position))
            {
                return turr;
            }
            
            return result;
        }

        static Pawn GetClosest(List<Pawn> _pawns, Pawn _me)
        {
            int best = int.MaxValue;
            Pawn result = null;

            foreach (Pawn p in _pawns)
            {
                if (_me.CanReach(p, PathEndMode.Touch, Danger.Deadly, true, TraverseMode.PassDoors))
                {
                    int dist = IntVec3Utility.ManhattanDistanceFlat(_me.PositionHeld, p.PositionHeld);

                    if (dist < best)
                    {
                        best = dist;
                        result = p;
                    }
                }
            }

            return result;
        }
    }
}
