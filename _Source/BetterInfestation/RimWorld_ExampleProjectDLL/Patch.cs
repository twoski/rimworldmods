﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Harmony;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using RimWorld;
using RimWorld.BaseGen;

namespace Infestation
{
    [StaticConstructorOnStartup]
    class Main
    {
        static Main()
        {
            var harmony = HarmonyInstance.Create("improvedinfestation");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(CompSpawnerHives))]
    [HarmonyPatch("CompTick")]
    [HarmonyPatch(new Type[] { })]
    class HiveTickPatch
    {
        static bool Prefix()
        {
            return false; //override
        }
    }

    [HarmonyPatch(typeof(CompSpawnerHives))]
    [HarmonyPatch("CompInspectStringExtra")]
    [HarmonyPatch(new Type[] { })]
    class HivePatch
    {
        static bool Prefix(CompSpawnerHives __instance, string __result)
        {
            __result = string.Empty;

            return false; //override
        }
    }

    [HarmonyPatch(typeof(SymbolResolver_Hives))]
    [HarmonyPatch("Resolve")]
    [HarmonyPatch(new Type[] {typeof(ResolveParams)})]
    class HiveSpawner
    {
        static bool Prefix(SymbolResolver_Hives __instance, ResolveParams rp)
        {
            IntVec3 intVec3;
            Hive hive;
            IntRange DefaultHivesCountRange = new IntRange(1, 3);
            if (!TryFindFirstHivePos(rp.rect, out intVec3))
            {
                return false;
            }
            int? nullable = rp.hivesCount;
            int num = (!nullable.HasValue ? DefaultHivesCountRange.RandomInRange : nullable.Value);
            Hive hive1 = (Hive)ThingMaker.MakeThing(ThingDefOf.Hive, null);
            hive1.SetFaction(Faction.OfInsects, null);
            if (rp.disableHives.HasValue && rp.disableHives.Value)
            {
                hive1.active = false;
            }

            hive1 = (Hive)GenSpawn.Spawn(hive1, intVec3, BaseGen.globalSettings.map, WipeMode.Vanish);

            CompSpawnerBugs bugs = hive1.GetComp<CompSpawnerBugs>();

            if (bugs != null)
            {
                bugs.SetActive(false);
            }

            for (int i = 0; i < num - 1; i++)
            {
                if (hive1.GetComp<CompSpawnerHives>().TrySpawnChildHive(true, out hive))
                {
                    hive1 = hive;

                    CompSpawnerBugs bugs1 = hive.GetComp<CompSpawnerBugs>();

                    if (bugs1 != null)
                    {
                        bugs1.SetActive(false);
                    }
                }
            }

            return false;
        }

        private static bool TryFindFirstHivePos(CellRect rect, out IntVec3 pos)
        {
            Map map = BaseGen.globalSettings.map;
            return (
                from mc in rect.Cells
                where mc.Standable(map)
                select mc).TryRandomElement<IntVec3>(out pos);
        }
    }

    [HarmonyPatch(typeof(GenStep_CaveHives))]
    [HarmonyPatch("Generate")]
    [HarmonyPatch(new Type[] { typeof(Map), typeof(GenStepParams) })]
    class CaveHiveSpawner
    {
        private static List<IntVec3> rockCells = new List<IntVec3>();

        private static List<IntVec3> possibleSpawnCells = new List<IntVec3>();

        private static List<Hive> spawnedHives = new List<Hive>();

        private const int MinDistToOpenSpace = 10;

        private const int MinDistFromFactionBase = 50;

        private const float CaveCellsPerHive = 1000f;

        public int SeedPart
        {
            get
            {
                return 349641510;
            }
        }

        static bool Prefix(GenStep_CaveHives __instance, Map map, GenStepParams parms)
        {
            Func<Thing, bool> func = null;
            if (!Find.Storyteller.difficulty.allowCaveHives)
            {
                return false;
            }

            MapGenFloatGrid caves = MapGenerator.Caves;
            MapGenFloatGrid elevation = MapGenerator.Elevation;
            float single = 0.7f;
            int num = 0;
            rockCells.Clear();

            foreach (IntVec3 allCell in map.AllCells)
            {
                if (elevation[allCell] > single)
                {
                    rockCells.Add(allCell);
                }
                if (caves[allCell] <= 0f)
                {
                    continue;
                }
                num++;
            }

            List<IntVec3> list = map.AllCells.Where<IntVec3>((IntVec3 c) => {
                IEnumerable<Thing> things = map.thingGrid.ThingsAt(c);
                if (func == null)
                {
                    func = (Thing thing) => thing.Faction != null;
                }
                return things.Any<Thing>(func);
            }).ToList<IntVec3>();

            GenMorphology.Dilate(list, 50, map, null);
            HashSet<IntVec3> intVec3s = new HashSet<IntVec3>(list);
            int num1 = GenMath.RoundRandom((float)num / 1000f);
            GenMorphology.Erode(rockCells, 10, map, null);
            possibleSpawnCells.Clear();

            for (int i = 0; i < rockCells.Count; i++)
            {
                if (caves[rockCells[i]] > 0f && !intVec3s.Contains(rockCells[i]))
                {
                    possibleSpawnCells.Add(rockCells[i]);
                }
            }

            spawnedHives.Clear();

            for (int j = 0; j < num1; j++)
            {
                TrySpawnHive(map);
            }

            spawnedHives.Clear();

            return false;
        }

        private static bool TryFindHiveSpawnCell(Map map, out IntVec3 spawnCell)
        {
            IntVec3 intVec3;
            float single = -1f;
            IntVec3 invalid = IntVec3.Invalid;
            int num = 0;
            while (num < 3)
            {
                if ((
                    from x in possibleSpawnCells
                    where (!x.Standable(map) || x.GetFirstItem(map) != null || x.GetFirstBuilding(map) != null ? false : x.GetFirstPawn(map) == null)
                    select x).TryRandomElement<IntVec3>(out intVec3))
                {
                    float single1 = -1f;
                    for (int i = 0; i < spawnedHives.Count; i++)
                    {
                        float squared = (float)intVec3.DistanceToSquared(spawnedHives[i].Position);
                        if (single1 < 0f || squared < single1)
                        {
                            single1 = squared;
                        }
                    }
                    if (!invalid.IsValid || single1 > single)
                    {
                        invalid = intVec3;
                        single = single1;
                    }
                    num++;
                }
                else
                {
                    break;
                }
            }
            spawnCell = invalid;
            return spawnCell.IsValid;
        }

        private static void TrySpawnHive(Map map)
        {
            IntVec3 intVec3;
            if (!TryFindHiveSpawnCell(map, out intVec3))
            {
                return;
            }

            possibleSpawnCells.Remove(intVec3);
            Hive hive = (Hive)GenSpawn.Spawn(ThingMaker.MakeThing(ThingDefOf.Hive, null), intVec3, map, WipeMode.Vanish);
            hive.SetFaction(Faction.OfInsects, null);
            hive.caveColony = true;

            (
                from x in hive.GetComps<CompSpawner>()
                where x.PropsSpawner.thingToSpawn == ThingDefOf.GlowPod
                select x).First<CompSpawner>().TryDoSpawn();

            hive.SpawnPawnsUntilPoints(Rand.Range(200f, 500f));
            hive.canSpawnPawns = false;
            hive.GetComp<CompSpawnerHives>().canSpawnHives = false;
            hive.GetComp<CompSpawnerBugs>().SetActive(false);

            spawnedHives.Add(hive);
        }
    }
}