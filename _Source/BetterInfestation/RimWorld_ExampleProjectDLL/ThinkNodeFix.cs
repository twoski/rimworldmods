﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse.AI;
using Verse;

namespace RimWorld
{
    public class ThinkNode_ConditionalHiveCanReproduce: ThinkNode_Conditional
    {
        public ThinkNode_ConditionalHiveCanReproduce()
        {
        }

        protected override bool Satisfied(Pawn pawn)
        {
            //Hive thing = pawn.mindState.duty.focus.Thing as Hive;
            //return (thing == null ? false : thing.GetComp<CompSpawnerHives>().canSpawnHives);
            return false;
        }
    }
}