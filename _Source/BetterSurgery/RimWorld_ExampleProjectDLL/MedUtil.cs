﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Reflection;
using Verse;

using UnityEngine;

using RimWorld;

namespace ImprovedSurgery
{
    public class MedUtil
    {
        public static bool CheckSurgeryFail(Pawn surgeon, Pawn patient, List<Thing> ingredients, BodyPartRecord part, RecipeDef recipe)
        {
            if (surgeon == null)
            {
                Log.Error("surgeon is null");
                return false;
            }

            if (patient == null)
            {
                Log.Error("patient is null");
                return false;
            }

            float value = 1f;
            value = value * surgeon.GetStatValue((!patient.RaceProps.IsMechanoid ? StatDefOf.MedicalSurgerySuccessChance : StatDefOf.MechanoidOperationSuccessChance), true);

            Room room = surgeon.GetRoom(RegionType.Set_Passable);
            if (room != null && !patient.RaceProps.IsMechanoid)
            {
                value = value * room.GetStat(RoomStatDefOf.SurgerySuccessChanceFactor);
            }

            value = value * GetAverageMedicalPotency(ingredients);
            value = value * recipe.surgerySuccessChanceFactor;

            if (Rand.Value <= value)
            {
                return false;
            }
            if (Rand.Value < recipe.deathOnFailedSurgeryChance)
            {
                int num = 0;
                while (!patient.Dead)
                {
                    HealthUtility.GiveInjuriesOperationFailureRidiculous(patient);
                    num++;
                    if (num <= 300)
                    {
                        continue;
                    }
                    Log.Error("Could not kill patient.");
                    break;
                }
            }
            else if (Rand.Value > 0.4f)
            {
                Messages.Message("MessageMedicalOperationFailureMinor".Translate(new object[] { surgeon.LabelShort, patient.LabelShort }), patient, MessageSound.Negative);
                HealthUtility.GiveInjuriesOperationFailureMinor(patient, part);
            }
            else if (Rand.Value > 0.1f)
            {
                Messages.Message("MessageMedicalOperationFailureCatastrophic".Translate(new object[] { surgeon.LabelShort, patient.LabelShort }), patient, MessageSound.SeriousAlert);
                HealthUtility.GiveInjuriesOperationFailureCatastrophic(patient, part);
            }
            else
            {
                if (Rand.Value > 0.6f)
                {
                    Messages.Message("MessageMedicalOperationFailureCatastrophic".Translate(new object[] { surgeon.LabelShort, patient.LabelShort }), patient, MessageSound.SeriousAlert);
                    HealthUtility.GiveInjuriesOperationFailureCatastrophic(patient, part);

                    DamageInfo? nullable = null;
                    patient.health.AddHediff(HediffDefOf.WoundInfection, part, nullable);
                }
                else
                {
                    Messages.Message("MessageMedicalOperationFailureRidiculous".Translate(new object[] { surgeon.LabelShort, patient.LabelShort }), patient, MessageSound.SeriousAlert);
                    HealthUtility.GiveInjuriesOperationFailureRidiculous(patient);
                }   
            }
            if (!patient.Dead)
            {
                MedUtil.TryGainBotchedSurgeryThought(patient, surgeon);
            }
            return true;
        }

        public static void SpawnIngredients(Pawn p, List<Thing> ingredients)
        {
            foreach (Thing t in ingredients)
            {
                Medicine med = t as Medicine;

                if (med == null)
                {
                    int hp = t.HitPoints - Rand.RangeInclusive(5, 10);

                    if (hp > 0)
                    {
                        Thing item = GenSpawn.Spawn(t.def, p.Position, p.Map);
                        item.HitPoints = hp;
                    }
                }
            }
        }

        static float GetAverageMedicalPotency(List<Thing> ingredients)
        {
            if (ingredients.NullOrEmpty<Thing>())
            {
                return 1f;
            }
            int num = 0;
            float statValue = 0f;
            for (int i = 0; i < ingredients.Count; i++)
            {
                Medicine item = ingredients[i] as Medicine;
                if (item != null)
                {
                    num = num + item.stackCount;
                    statValue = statValue + item.GetStatValue(StatDefOf.MedicalPotency, true) * (float)item.stackCount;
                }
            }
            if (num == 0)
            {
                return 1f;
            }
            return statValue / (float)num;
        }

        static void TryGainBotchedSurgeryThought(Pawn patient, Pawn surgeon)
        {
            if (!patient.RaceProps.Humanlike)
            {
                return;
            }
            patient.needs.mood.thoughts.memories.TryGainMemory(ThoughtDefOf.BotchedMySurgery, surgeon);
        }
    }
}
