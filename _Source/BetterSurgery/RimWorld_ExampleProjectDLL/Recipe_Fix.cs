﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Reflection;
using Verse;

using RimWorld;

using HugsLib.Source.Detour;

namespace ImprovedSurgery
{
    public static class Recipe_Fix
    {
        [DetourMethod(typeof(Recipe_InstallArtificialBodyPart), "ApplyOnPawn")]
        public static void InstallArtificialPart(this Recipe_InstallArtificialBodyPart rec, Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients)
        {
            if (billDoer != null)
            {
                if (MedUtil.CheckSurgeryFail(billDoer, pawn, ingredients, part, rec.recipe))
                {
                    MedUtil.SpawnIngredients(billDoer, ingredients);
                    return;
                }

                TaleRecorder.RecordTale(TaleDefOf.DidSurgery, new object[] { billDoer, pawn });
                //MedUtil.RestorePartAndSpawnAllPreviousParts(pawn, part, billDoer.Position);

                var MedicalRecipesUtility = Type.GetType("RimWorld.MedicalRecipesUtility, Assembly-CSharp");
                var restore = MedicalRecipesUtility.GetMethod("RestorePartAndSpawnAllPreviousParts", BindingFlags.Static | BindingFlags.Public);

                if (restore != null)
                {
                    restore.Invoke(MedicalRecipesUtility, new object[] { pawn, part, billDoer.Position, pawn.Map });
                }
                else
                {
                    Log.ErrorOnce("Unable to reflect MedicalRecipesUtility.RestorePartAndSpawnAllPreviousParts!", 305432421);
                }
            }

            pawn.health.AddHediff(rec.recipe.addsHediff, part, null);
        }

        [DetourMethod(typeof(Recipe_RemoveHediff), "ApplyOnPawn")]
        public static void RemoveHediff(this Recipe_RemoveHediff rec, Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients)
        {
            string str;
            if (billDoer != null)
            {
                if (MedUtil.CheckSurgeryFail(billDoer, pawn, ingredients, part, rec.recipe))
                {
                    return;
                }

                TaleRecorder.RecordTale(TaleDefOf.DidSurgery, new object[] { billDoer, pawn });

                if (PawnUtility.ShouldSendNotificationAbout(pawn) || PawnUtility.ShouldSendNotificationAbout(billDoer))
                {
                    str = (rec.recipe.successfullyRemovedHediffMessage.NullOrEmpty() ? "MessageSuccessfullyRemovedHediff".Translate(new object[] { billDoer.LabelShort, pawn.LabelShort, rec.recipe.removesHediff.label }) : string.Format(rec.recipe.successfullyRemovedHediffMessage, billDoer.LabelShort, pawn.LabelShort));
                    Messages.Message(str, pawn, MessageSound.Benefit);
                }
            }
            Hediff hediff = pawn.health.hediffSet.hediffs.Find((Hediff x) => (x.def != rec.recipe.removesHediff ? false : x.Part == part));
            pawn.health.RemoveHediff(hediff);
        }

        [DetourMethod(typeof(Recipe_InstallImplant), "ApplyOnPawn")]
        public static void InstallImplant(this Recipe_InstallImplant rec, Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients)
        {
            if (billDoer != null)
            {
                if (MedUtil.CheckSurgeryFail(billDoer, pawn, ingredients, part, rec.recipe))
                {
                    MedUtil.SpawnIngredients(billDoer, ingredients);
                    return;
                }

                TaleRecorder.RecordTale(TaleDefOf.DidSurgery, new object[] { billDoer, pawn });
            }
            pawn.health.AddHediff(rec.recipe.addsHediff, part, null);
        }

        [DetourMethod(typeof(Recipe_InstallNaturalBodyPart), "ApplyOnPawn")]
        public static void InstallNaturalPart(this Recipe_InstallNaturalBodyPart rec, Pawn pawn, BodyPartRecord part, Pawn billDoer, List<Thing> ingredients)
        {
            if (billDoer != null)
            {
                if (MedUtil.CheckSurgeryFail(billDoer, pawn, ingredients, part, rec.recipe))
                {
                    MedUtil.SpawnIngredients(billDoer, ingredients);
                    return;
                }

                TaleRecorder.RecordTale(TaleDefOf.DidSurgery, new object[] { billDoer, pawn });
                //MedUtil.RestorePartAndSpawnAllPreviousParts(pawn, part, billDoer.Position);

                var MedicalRecipesUtility = Type.GetType("RimWorld.MedicalRecipesUtility, Assembly-CSharp");
                var restore = MedicalRecipesUtility.GetMethod("RestorePartAndSpawnAllPreviousParts", BindingFlags.Static | BindingFlags.Public);

                if (restore != null)
                {
                    restore.Invoke(MedicalRecipesUtility, new object[] { pawn, part, billDoer.Position, pawn.Map });
                }
                else
                {
                    Log.ErrorOnce("Unable to reflect MedicalRecipesUtility.RestorePartAndSpawnAllPreviousParts!", 305432421);
                }
            }
        }
    }
}
