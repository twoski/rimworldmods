﻿
using System;
using System.Collections.Generic;
using System.Linq;

using RimWorld;

namespace Verse.AI
{
    public class MentalState_Antisocial: MentalState
    {
        bool initialized = false;
        bool reached = false;
        IntVec3 target_pos;

        public void Initialize(Pawn p)
        {
            if (initialized)
            {
                return;
            }

            List<Pawn> pawns = FindPawns(p);
            IntVec3 pos = DropCellFinder.RandomDropSpot(p.Map);//RandomDropSpot();
            int max_tries = 50;
            int tries = 0;

            while (!Validate(pos, pawns))
            {
                tries++;
                pos = DropCellFinder.RandomDropSpot(p.Map);

                if (tries > max_tries)
                {
                    bool failed = true;

                    foreach (IntVec3 c in p.Map.AllCells)
                    {
                        if (!p.Map.areaManager.Home[c])
                        {
                            pos = c;
                            failed = false;
                            break;
                        }
                    }

                    if (failed)
                    {
                        break;
                    }
                }
            }

            target_pos = pos;
            reached = false;
            initialized = true;
        }

        List<Pawn> FindPawns(Pawn pawn)
        {
            List<Pawn> allPawns = pawn.Map.mapPawns.AllPawnsSpawned;
            List<Pawn> valid = new List<Pawn>();

            foreach (Pawn p in allPawns)
            {
                if (p.DestroyedOrNull() || p.Downed || p.Dead)
                {
                    continue;
                }

                if (p.IsPrisonerOfColony || p.IsColonist || p.IsColonistPlayerControlled)
                {
                    valid.Add(p);
                }
            }

            return valid;
        }

        bool Validate(IntVec3 pos, List<Pawn> pawns)
        {
            foreach (Pawn p in pawns)
            {
                int dist = IntVec3Utility.ManhattanDistanceFlat(pos, p.PositionHeld);

                if (dist < 50)
                {
                    return false;
                }
            }

            return !Find.CurrentMap.areaManager.Home[pos];
        }

        public void SetReached(bool r)
        {
            reached = r;
        }

        public bool ReachedPos()
        {
            return reached;
        }

        public IntVec3 GetTargetPos()
        {
            return target_pos;
        }

        public override RandomSocialMode SocialModeMax()
        {
            return RandomSocialMode.Off;
        }
    }
}
