﻿using System;
using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
    public class JobGiver_Antisocial: ThinkNode_JobGiver
    {
        protected bool IgnoreForbid(Pawn pawn)
        {
            return pawn.InMentalState;
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            ((MentalState_Antisocial)pawn.MentalState).Initialize(pawn);

            IntVec3 target = ((MentalState_Antisocial)pawn.MentalState).GetTargetPos();

            if (CloseToPoint(pawn, target))
            {
                ((MentalState_Antisocial)pawn.MentalState).SetReached(true);
            }

            if (target == null || ((MentalState_Antisocial)pawn.MentalState).ReachedPos())
            {
                return null;
            }

            return new Job(JobDefOf.GotoWander, target);
        }

        bool CloseToPoint(Pawn p, IntVec3 pos)
        {
            int dist = IntVec3Utility.ManhattanDistanceFlat(p.PositionHeld, pos);

            return dist < 15;
        }
    }
}
