﻿using System;
using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
    public class JobGiver_Apathy: JobGiver_Wander
    {
        public JobGiver_Apathy()
        {
            this.wanderRadius = 10f;
            this.ticksBetweenWandersRange = new IntRange(100, 200);
            this.locomotionUrgency = LocomotionUrgency.Walk;
        }

        protected override IntVec3 GetWanderRoot(Pawn pawn)
        {
            return pawn.Position;
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            pawn.mindState.nextMoveOrderIsWait = !pawn.mindState.nextMoveOrderIsWait;

            if (pawn.mindState.nextMoveOrderIsWait)
            {
                return new Job(JobDefOf.Wait_Wander)
                {
                    expiryInterval = this.ticksBetweenWandersRange.RandomInRange
                };
            }

            IntVec3 exactWanderDest = this.GetExactWanderDest(pawn);

            if (!exactWanderDest.IsValid || Rand.Range(0f, 1f) < 0.5f)
            {
                return null;
            }

            Job j = new Job(JobDefOf.GotoWander, exactWanderDest)
            {
                locomotionUrgency = this.locomotionUrgency
            };

            pawn.Map.pawnDestinationReservationManager.Reserve(pawn, j, exactWanderDest);

            if (Rand.Range(0f, 1f) < 0.5f)
            {
                this.locomotionUrgency = LocomotionUrgency.Amble;
            }
            else
            {
                this.locomotionUrgency = LocomotionUrgency.Walk;
            }

            return j;
        }
    }
}
