﻿using System;
using System.Collections.Generic;
using Verse;
using Verse.AI;

using UnityEngine;

namespace RimWorld
{
    public class JobGiver_Compulsion: ThinkNode_JobGiver
    {
        protected bool IgnoreForbid(Pawn pawn)
        {
            return pawn.InMentalState;
        }

        protected override Job TryGiveJob(Pawn pawn)
        {
            if (Rand.Range(0f, 1f) < 0.5f)
            {
                List<Thing> filth = pawn.Map.listerFilthInHomeArea.FilthInHomeArea;
                Thing t = GetClosestFilth(pawn, filth);

                if (CanClean(pawn, t))
                {
                    return new Job(JobDefOf.Clean, t);
                }
            }

            if (Rand.Range(0f, 1f) < 0.5f)
            {
                Predicate<Thing> validator = (Thing t) => !t.IsForbidden(pawn) && HaulAIUtility.PawnCanAutomaticallyHaulFast(pawn, t, true);
                Thing thing = GenClosest.ClosestThing_Global_Reachable(pawn.Position, pawn.Map, pawn.Map.listerHaulables.ThingsPotentiallyNeedingHauling(), PathEndMode.OnCell, TraverseParms.For(pawn, Danger.Deadly, TraverseMode.ByPawn, false), 9999f, validator, null);

                if (thing != null)
                {
                    return HaulAIUtility.HaulToStorageJob(pawn, thing);
                }
            }

            return null;
        }

        /*bool IsIncapable(Pawn p, WorkTypeDef work)
        {
            if (p.story.DisabledWorkTypes.Contains(work))
            {
                return true;
            }

            for (int i = 0; i < work.workGiversByPriority.Count; i++)
            {
                bool flag = true;
                for (int j = 0; j < work.workGiversByPriority[i].requiredCapacities.Count; j++)
                {
                    PawnCapacityDef activity = work.workGiversByPriority[i].requiredCapacities[j];
                    if (!p.health.capacities.CapableOf(activity))
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    return false;
                }
            }
            return true;
        }*/

        Thing GetClosestFilth(Pawn pawn, List<Thing> filth)
        {
            Thing result = null;
            int minDist = int.MaxValue;

            foreach (Thing t in filth)
            {
                int dist = IntVec3Utility.ManhattanDistanceFlat(pawn.PositionHeld, t.PositionHeld);

                if (dist < minDist)
                {
                    minDist = dist;
                    result = t;
                }
            }

            return result;
        }

        bool CanClean(Pawn pawn, Thing t)
        {
            Filth filth = t as Filth;

            return filth != null && pawn.Map.areaManager.Home[filth.Position] && pawn.CanReserveAndReach(t, PathEndMode.ClosestTouch, Danger.Deadly, 1);
        }
    }
}
