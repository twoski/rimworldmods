﻿
using System;
using System.Collections.Generic;
using System.Linq;

using RimWorld;
using UnityEngine;

namespace Verse.AI
{
    public class MentalState_SelfHarm: MentalState
    {
        public override void PostStart(string reason)
        {
            base.PostStart(reason);

            foreach (BodyPartRecord r in pawn.health.hediffSet.GetNotMissingParts())
            {
                if (r.def == BodyPartDefOf.Hand)
                {
                    float arm_hp = pawn.health.hediffSet.GetPartHealth(r);
                    int dmg_amt = Mathf.RoundToInt(arm_hp * Rand.Range(0.20f, 0.35f));

                    DamageInfo dmg = new DamageInfo(DamageDefOf.Cut, dmg_amt, 0.0f, -1.0f, pawn, r);
                    pawn.TakeDamage(dmg);
                }
            }
        }

        public override RandomSocialMode SocialModeMax()
        {
            return RandomSocialMode.Off;
        }
    }
}
