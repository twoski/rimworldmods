﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
using UnityEngine;

namespace RimWorld
{

    public class IncidentWorker_MeteorShower : IncidentWorker
    {
        private const float FogClearRadius = 5f;

        public override bool TryExecute(IncidentParms parms)
        {
            IntVec3 pos = RCellFinder.RandomDropSpot();

            Find.History.AddGameEvent("MeteorShower".Translate(), GameEventType.BadNonUrgent, true, pos);

            MeteorUtility.MakeMeteorAt(pos);

            return true;
        }
    }
}
