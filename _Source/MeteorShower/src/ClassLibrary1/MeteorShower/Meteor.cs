﻿using System;
using Verse;
using Verse.Sound;

namespace RimWorld
{
    public class Meteor: Thing
    {
        public override void ExposeData()
        {
            base.ExposeData();
        }

        public override void Tick()
        {
        }

        public override void Destroy(DestroyMode mode = DestroyMode.Vanish)
        {
            base.Destroy(mode);

            for (int i = 0; i < 4; i++)
            {
                Thing thing = ThingMaker.MakeThing(ThingDef.Named("RockRubble"));
                GenPlace.TryPlaceThing(thing, base.Position, ThingPlaceMode.Near);
            }
        }
    }
}
