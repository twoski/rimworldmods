﻿using System;
using UnityEngine;
using Verse;
using Verse.Sound;
//using VerseBase;

namespace RimWorld
{
    public class MeteorFlight: Thing
    {
        protected const int MinTicksToImpact = 120;
        protected const int MaxTicksToImpact = 200;
        private const int SoundAnticipationTicks = 100;
        protected int ticksToImpact = 120;
        private bool soundPlayed;
        private static readonly SoundDef LandSound = SoundDef.Named("DropPodFall");
        private static readonly SoundDef ExplodeSound = SoundDef.Named("Explosion_Bomb");
        //private static readonly Material ShadowMat = MaterialPool.MatFrom("Things/Special/DropPodShadow", ShaderDatabase.Transparent);

        public override Vector3 DrawPos
        {
            get
            {
                Vector3 result = base.Position.ToVector3ShiftedWithAltitude(AltitudeLayer.FlyingItem);
                float num = (float) (this.ticksToImpact * this.ticksToImpact) * 0.01f;
                result.x -= num * 0.4f;
                result.z += num * 0.6f;
                return result;
            }
        }

        public override void SpawnSetup()
        {
            base.SpawnSetup();
            this.ticksToImpact = Rand.RangeInclusive(120, 200);
            if (Find.RoofGrid.Roofed(base.Position))
            {
                Log.Warning("Meteor dropped on roof at " + base.Position);
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.LookValue<int>(ref this.ticksToImpact, "ticksToImpact", 0, false);
        }

        public override void Tick()
        {
            this.ticksToImpact--;

            if (this.ticksToImpact <= 0)
            {
                this.Impact();
            }
            if (!this.soundPlayed && this.ticksToImpact < 100)
            {
                this.soundPlayed = true;
                MeteorFlight.LandSound.PlayOneShot(base.Position);
            }
        }

        public override void DrawAt(Vector3 drawLoc)
        {
            base.DrawAt(drawLoc);
            float num = 2f + (float) this.ticksToImpact / 100f;
            Vector3 s = new Vector3(num, 1f, num);
            Matrix4x4 matrix = default(Matrix4x4);
            drawLoc.y = Altitudes.AltitudeFor(AltitudeLayer.Shadows);
            matrix.SetTRS(this.TrueCenter(), base.Rotation.AsQuat, s);
            //Graphics.DrawMesh(MeshPool.plane10Back, matrix, Meteor.ShadowMat, 0);
        }

        private void Impact()
        {
            MeteorFlight.ExplodeSound.PlayOneShot(base.Position);
            Meteorite meteor = (Meteorite) ThingMaker.MakeThing(ThingDef.Named("Meteorite"));

            GenSpawn.Spawn(meteor, base.Position, base.Rotation);

            int explosionSize = UnityEngine.Random.Range(3, 6);

            // Make the crash explosion
            GenExplosion.DoExplosion(base.Position, explosionSize, DamageDefOf.Bomb, null);
            GenExplosion.DoExplosion(base.Position, explosionSize, DamageDefOf.Flame, null);

            for (int i = 0; i < UnityEngine.Random.Range(1, 3); i++) // randomize the fire a bit
            {
                IntVec3 pos = base.Position + new IntVec3(base.Position.x + UnityEngine.Random.Range(-4, 4), base.Position.y + UnityEngine.Random.Range(-4, 4), base.Position.z);

                GenExplosion.DoExplosion(pos, UnityEngine.Random.Range(2, 4), DamageDefOf.Flame, null);
            }

            // camera goes ham
            Find.CameraMap.shaker.DoShake(1.5f);

            this.Destroy(DestroyMode.Vanish);
        }
    }
}