﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace RimWorld
{
    public class Meteorite: Building
    {
        private int spawnticks = 1000;

        public override void SpawnSetup()
        {
            base.SpawnSetup();
        }

        public override void Tick()
        {
            base.Tick();
        }

        public override void Destroy(DestroyMode mode = DestroyMode.Vanish)
        {
            base.Destroy(mode);

            Thing uranium = ThingMaker.MakeThing(ThingDef.Named("Uranium"), null);
            Thing spawned = GenSpawn.Spawn(uranium, this.Position);

            spawned.stackCount = 75;
            /*for (int i = 0; i < 4; i++)
            {
                Thing thing = ThingMaker.MakeThing(ThingDef.Named("RockRubble"));
                GenPlace.TryPlaceThing(thing, base.Position, ThingPlaceMode.Near);
            }*/
        }
    }
}